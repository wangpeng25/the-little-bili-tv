# 哔哩哔哩小电视2.0
![输入图片说明](images/blbl.jpg)
#### 支持功能  

- 微信配网（完成）  
- 时间显示（完成）  
- 三日天气显示（完成）  
- 温湿度显示（完成）  
- b站粉丝数显示（完成）  
- 基金走势显示（待完善）  
- web设置（完成）  
- 温湿度mqtt上报（考虑中）  
- 电脑性能显示（待完善）  
- 事件提醒（待完善）  
- 微信小程序支持（考虑中）  
- 电脑屏幕显示（考虑中）
   
#### 文件分布

- main为开始文件夹，存放lvgl界面文件  
- components中存放驱动程序  
- html文件夹中为spiffs文件系统文件，存放烧录到esp32中的网页  
- pcb文件为原理图及制作文件  

#### 文件资源
固件及相关软件文件链接：https://pan.baidu.com/s/1boCcqZjzDPuFvZM_3Qj4cA#list/path=%2F  
提取码：xxyd  
#### pcb原理图地址：  
上层：https://oshwhub.com/xiaoxiaoyudu/bi-li-bi-li-xiao-dian-shi2-0  
下层：https://oshwhub.com/xiaoxiaoyudu/bi-li-bi-li-xiao-dian-shi2-5-ban-ben    
#### 图片转换工具源作者github地址  
https://github.com/zhangjingxun12/Lvgl_image_convert_tool  
#### 字体转换工具使用阿里兄工具  
http://www.lfly.xyz/forum.php  
#### 视频演示地址 
新版https://www.bilibili.com/video/BV1Xb4y1i7KE  
旧版https://www.bilibili.com/video/BV1zh411Z7DV  
#### 开发工具 
采用vscode软件开发，安装espidf插件，idf版本为4.3 

