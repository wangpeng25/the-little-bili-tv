

#ifndef _PAGE3_
#define _PAGE3_

#ifdef __cplusplus
extern "C" {
#endif
/*********************
* INCLUDES
*********************/
#ifdef LV_CONF_INCLUDE_SIMPLE
#include "lvgl.h"
#else
#include "../../lvgl/lvgl.h"
#endif
void page_set_start(void);
void page_set_end(void);
#ifdef __cplusplus
} /* extern "C" */
#endif




#endif // _TEST_


