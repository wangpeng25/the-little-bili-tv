#include "task.h"
#include "adc.h"
#include "http.h"
#include "gui_time.h"
#include "../blbl_sys.h"
extern struct _blbl_sys_t blbltv_t;
extern SemaphoreHandle_t xGuiSemaphore;
static void adcTask(void *pvParameter)
{
    uint32_t value;
    uint8_t light;
    uint8_t last_light=0;
    uint8_t set_light=0;
    while(1){
        value=get_adc_once();
        light=(100-value*100/4096);
        if(blbltv_t.timeinfo_t.tm_hour>20||blbltv_t.timeinfo_t.tm_hour<8){//20点之后，8点之前背光调整
           if(light<28){
               if(last_light!=light){
                    blbl_set_scr_backlight(0);
               }
           }else if(light<32){
               if(last_light!=light){
                    blbl_set_scr_backlight(20);
               }
           }else{
               if(last_light!=light){
                    blbl_set_scr_backlight(50);
               }
           }
           
        }else{
            if(set_light!=blbltv_t.backlight){
                if(blbltv_t.backlight==0){//关屏
                    blbltv_t.backlight=20;
                }
                blbl_set_scr_backlight(blbltv_t.backlight);
                set_light=blbltv_t.backlight;
            }
        }
        if(last_light!=light)
        ESP_LOGI("TEST","ADC:%d",light);
        last_light=light;
        vTaskDelay(200);
    }
}
extern EventGroupHandle_t s_wifi_event_group;//wifi事件组
extern const int CONNECTED_BIT;
void blbl_task(void *arg)//时间任务，一直在lvgl后台运行
{
	static uint32_t t=1;
    uint8_t state=1;
    EventBits_t uxBits;
	while(1)
	{
#ifdef USE_SHT30
		if(t%(2*5)==0)//获取温湿度
	 	sht30_get_value();
#endif         
         //wifi状态检测
	    uxBits=xEventGroupGetBits(s_wifi_event_group);
        if(uxBits & CONNECTED_BIT)
            state=1;
        else
            state=0;
        if(state!=blbltv_t.state_t.wifi){
            if(state){//连接网络
                ESP_LOGI("TASK","WIFI_CONCONT");
            }else{
                ESP_LOGI("TASK","WIFI_DISCONCONT");
            }
            blbltv_t.state_t.wifi=state;
            if (pdTRUE == xSemaphoreTake(xGuiSemaphore, portMAX_DELAY)) {
                lv_ico_show(&blbltv_t.head_t.wifi ,2+state);
                xSemaphoreGive(xGuiSemaphore);
            }
        }
        //  网络部分
        if(blbltv_t.state_t.wifi==1){//wifi连接
		    if(t%(2*60*3)==0){//3分钟 粉丝数
                ESP_LOGI("TASK","b站网络请求");
		    	get_fans();
		    }
		    if(t==(2*60)){//34分钟 天气
                ESP_LOGI("TASK","天气网络请求");
		    	get_weather();
		    }
        }
		t++;
        if(t==(2*60*35))
            t=0;     
		vTaskDelay(500/portTICK_RATE_MS);
	}
}
void time_task(void *arg)//时间任务，一直在lvgl后台运行
{
    char str[20];
    uint8_t ram=0;
	while(1){
    	get_time(&(blbltv_t.timeinfo_t));//获取时间
        //剩余内存   
        if(ram!=esp_get_free_heap_size()/1024){
            ram=esp_get_free_heap_size()/1024;
            if (pdTRUE == xSemaphoreTake(xGuiSemaphore, portMAX_DELAY)) {
                sprintf(str,"%dk",ram);
                hand_ram_show(str);
                xSemaphoreGive(xGuiSemaphore);
            }
        }
        vTaskDelay(500/portTICK_RATE_MS);
	}
}
void task_start()
{
    xTaskCreate(&adcTask, "adc", 1048*2, NULL, 2, NULL);
    xTaskCreate(&blbl_task, "blbl_task", 1048*5, NULL, 8, NULL);
    xTaskCreate(&time_task, "time_task", 1048*2, NULL, 3, NULL);
}