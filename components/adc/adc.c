#include "adc.h"

#define DEFAULT_VREF    1100        //Use adc2_vref_to_gpio() to obtain a better estimate
#define NO_OF_SAMPLES   64          //Multisampling

static esp_adc_cal_characteristics_t *adc_chars;
static const adc_channel_t channel = ADC1_CHANNEL_4;     //GPIO34 if ADC1
static const adc_atten_t atten = ADC_ATTEN_11db;//满量程为3.9v
static const adc_unit_t unit = ADC_UNIT_1;//选择adc1

static void check_efuse()
{
    //Check TP is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {//检查ADC校准值是否已刻录到eFuse中。该功能检查ADC参考电压或两点值是否已烧至当前ESP32的eFuse

        printf("eFuse Two Point: Supported\n");
    } else {
        printf("eFuse Two Point: NOT supported\n");
    }

    //Check Vref is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
        printf("eFuse Vref: Supported\n");
    } else {
        printf("eFuse Vref: NOT supported\n");
    }
}

static void print_char_val_type(esp_adc_cal_value_t val_type)
{
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        printf("Characterized using Two Point Value\n");
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        printf("Characterized using eFuse Vref\n");
    } else {
        printf("Characterized using Default Vref\n");
    }
}
void adc_init()
{
    check_efuse();
    adc1_config_width(ADC_WIDTH_BIT_12);//采集宽度
    adc1_config_channel_atten(channel, atten);//配置通道 以及衰减度

    //Characterize ADC
    adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);//在特定衰减下表征ADC的特性
    print_char_val_type(val_type);
}
uint32_t get_adc_once()
{
        uint32_t adc_reading = 0;
        //Multisampling
        for (int i = 0; i < NO_OF_SAMPLES; i++) {            
            adc_reading += adc1_get_raw((adc1_channel_t)channel);//从单个通道获取ADC1读数。
        }
        adc_reading /= NO_OF_SAMPLES;
        //Convert adc_reading to voltage in mV
        uint32_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);//将ADC读数转换为以mV为单位的电压
        return voltage;
} 