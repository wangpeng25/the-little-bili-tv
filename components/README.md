# 驱动文件

#### 文件结构
adc为电池电量采集文件夹，后来发现电池容量太小，并没有使用
button为按键驱动
dht11为dht11驱动，使用github开源驱动
font为字库文件
http为请求http网页文件夹
lcd为解析png和jpg文件库，原始方案使用，之后放弃
lv_example为lvgl官方例程
photo为天气图片文件等
read_nvs为读写nvs库
smqtt为mqtt库
time为获取时间库
web为网页服务，参考官方例程
wificonnet为wifi连接库
